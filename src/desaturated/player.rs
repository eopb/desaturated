use amethyst::{
    assets::Handle,
    core::transform::Transform,
    ecs::prelude::{Component, DenseVecStorage},
    prelude::*,
    renderer::{SpriteRender, SpriteSheet},
};

pub struct Player;

impl Component for Player {
    type Storage = DenseVecStorage<Self>;
}

impl Player {
    pub fn initialise(world: &mut World, sprite_sheet: Handle<SpriteSheet>) {
        let sprite_render = SpriteRender {
            sprite_sheet: sprite_sheet.clone(),
            sprite_number: 0,
        };

        let mut transform = Transform::default();

        transform.set_translation_xyz(50.0, 50.0, 0.0);

        world
            .create_entity()
            .with(sprite_render.clone())
            .with(Player)
            .with(transform)
            .build();
    }
}
